@Api
Feature: Test Twitter Tweets

  Scenario: 01. Get recent tweets from our Home Timeline
    Given I post a tweet of "Wow! I am tweeting via Java and RestAssured. Much wow!"
    When I retrieve the resource of "/home_timeline.json"
    Then the "[0].text" key has a value of "Wow! I am tweeting via Java and RestAssured. Much wow!"