package utils.hooks;

import apis.BaseApiTests;
import cucumber.api.java.Before;

import java.io.UnsupportedEncodingException;

public class CucumberHooks {

    @Before("@Api")
    public void setBaseUri() throws UnsupportedEncodingException {
        BaseApiTests.setBaseUri();
    }
}
