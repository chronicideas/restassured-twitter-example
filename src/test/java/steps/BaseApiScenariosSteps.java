package steps;

import apis.BaseApiTests;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BaseApiScenariosSteps {

    @Given("^I post a tweet of \"([^\"]*)\"$")
    public void i_post_a_tweet_of(String message) throws Throwable {
        BaseApiTests.postTweet(message);
    }

    @When("^I retrieve the resource of \"([^\"]*)\"$")
    public void i_retrieve_the_resource_of(String apiResource) throws Throwable {
        BaseApiTests.getRequest(apiResource);
    }

    @Then("^the \"([^\"]*)\" key has a value of \"([^\"]*)\"$")
    public void the_key_has_a_value_of(String key, String value) throws Throwable {
        BaseApiTests.assertKeyValue(key, value);
    }
}
